using MediatR;
using Microsoft.AspNetCore.Mvc;
using Optimum.Hms.Inventory.Api.Dto;
using Optimum.Hms.Inventory.Application.Supplier.Commands;
using Optimum.Hms.Inventory.Application.Supplier.Query.AllSuppliers;

namespace Optimum.Hms.Inventory.Api.Controllers
{
    [Route("[controller]")]
    public class SupplierController : Controller
    {
        private readonly IMediator _mediator;

        public SupplierController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpPost]
        public IActionResult Post()
        {
            CreateSupplierCommand createSupplierCommand = new CreateSupplierCommand();
            createSupplierCommand.Name = "Beres";
            createSupplierCommand.TaxId = "3254";
            var result = _mediator.Send(createSupplierCommand);
            return Ok(result);
        }
        [HttpPost("create-company-supplier")]
        public IActionResult Post(CompanySupplierDto companySupplierDto)
        {
            CreateCompanySupplierCommand createCompanySupplierCommand = new CreateCompanySupplierCommand();
            createCompanySupplierCommand.CompanyName = companySupplierDto.CompanyName;
            createCompanySupplierCommand.EmailAddress = companySupplierDto.EmailAddress;
            createCompanySupplierCommand.EmailCategory = companySupplierDto.EmailCategory;
            createCompanySupplierCommand.TaxId = companySupplierDto.TaxId;
            createCompanySupplierCommand.AddressLine1 = companySupplierDto.AddressLine1;
            createCompanySupplierCommand.AddressLine2 = companySupplierDto.AddressLine2;
            createCompanySupplierCommand.City = companySupplierDto.City;
            createCompanySupplierCommand.State = companySupplierDto.State;
            createCompanySupplierCommand.Country = companySupplierDto.Country;
            createCompanySupplierCommand.Zip = companySupplierDto.Zip;
            createCompanySupplierCommand.Phones = companySupplierDto.Phones;
            createCompanySupplierCommand.PhoneCategories = companySupplierDto.PhoneCategories;
            createCompanySupplierCommand.Comment = companySupplierDto.Comment;

            var result = _mediator.Send(createCompanySupplierCommand);
            return Ok(result);
        }
        [HttpPost("create-individual-supplier")]
        public IActionResult Post(IndividualSupplierDto individualSupplierDto)
        {
            CreateIndividualSupplierCommand createIndividualSupplierCommand = new CreateIndividualSupplierCommand();
            createIndividualSupplierCommand.FirstName = individualSupplierDto.FirstName;
            createIndividualSupplierCommand.LastName = individualSupplierDto.LastName;
            createIndividualSupplierCommand.EmailAddress = individualSupplierDto.EmailAddress;
            createIndividualSupplierCommand.JobPosition = individualSupplierDto.JobPosition;
            createIndividualSupplierCommand.Title = individualSupplierDto.Title;
            createIndividualSupplierCommand.CompanyName = individualSupplierDto.CompanyName;
            createIndividualSupplierCommand.TaxId = individualSupplierDto.TaxId;
            var result = _mediator.Send(createIndividualSupplierCommand);
            return Ok(result);
        }
        [HttpGet]
        public IActionResult Get()
        {
            var result = _mediator.Send(new AllSuppliersQuery());
            return Ok(result);
        }

    }
}