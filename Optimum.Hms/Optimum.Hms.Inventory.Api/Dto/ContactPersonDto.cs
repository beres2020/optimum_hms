﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimum.Hms.Inventory.Api.Dto
{
    public class ContactPersonDto
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string JobPosition { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
