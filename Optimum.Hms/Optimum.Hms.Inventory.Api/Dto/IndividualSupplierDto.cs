﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimum.Hms.Inventory.Api.Dto
{
    public class IndividualSupplierDto
    {
        public string Id { get; set; }
        public int SupplierTypeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string JobPosition { get; set; }
        public string Title { get; set; }
        public string CompanyName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string[] Phones { get; set; }
        public string TaxId { get; set; }
        public string Comment { get; set; }
    }
}
