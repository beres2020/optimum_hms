﻿using MediatR;
using Optimum.Hms.Inventory.Common.Services.Supplier;
using Optimum.Hms.Inventory.Common.Services.Supplier.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Optimum.Hms.Inventory.Application.Supplier.Commands
{
    public class CreateCompanySupplierCommandHandler : IRequestHandler<CreateCompanySupplierCommand, bool>
    {
        private readonly ICompanySupplierCommandService _companySupplierCommandService;
        public CreateCompanySupplierCommandHandler(ICompanySupplierCommandService companySupplierCommandService)
        {
            _companySupplierCommandService = companySupplierCommandService;
        }
        public Task<bool> Handle(CreateCompanySupplierCommand request, CancellationToken cancellationToken)
        {
            CompanySupplierCreateModel companySupplierCreateModel = new CompanySupplierCreateModel();
            companySupplierCreateModel.CompanyName = request.CompanyName;
            companySupplierCreateModel.EmailAddress = request.EmailAddress;
            companySupplierCreateModel.EmailCategory = request.EmailCategory;
            companySupplierCreateModel.TaxId = request.TaxId;
            companySupplierCreateModel.AddressLine1 = request.AddressLine1;
            companySupplierCreateModel.AddressLine2 = request.AddressLine2;
            companySupplierCreateModel.City = request.City;
            companySupplierCreateModel.State = request.State;
            companySupplierCreateModel.Country = request.Country;
            companySupplierCreateModel.Zip = request.Zip;
            companySupplierCreateModel.Phones = request.Phones;
            companySupplierCreateModel.PhoneCategories = request.PhoneCategories;
            companySupplierCreateModel.Comment = request.Comment;

            var result = _companySupplierCommandService.CreateCompanySupplier(companySupplierCreateModel);
            return Task.FromResult(true);
        }
    }
}
