﻿using MediatR;
using Optimum.Hms.Inventory.Common.Services.Supplier;
using Optimum.Hms.Inventory.Common.Services.Supplier.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Optimum.Hms.Inventory.Application.Supplier.Commands
{
    public class CreateIndividualSupplierCommandHandler : IRequestHandler<CreateIndividualSupplierCommand, bool>
    {
        private readonly IIndividualSupplierCommandService _iIndividualSupplierCommandService;
        public CreateIndividualSupplierCommandHandler(IIndividualSupplierCommandService iIndividualSupplierCommandService)
        {
            _iIndividualSupplierCommandService = iIndividualSupplierCommandService;
        }
        public Task<bool> Handle(CreateIndividualSupplierCommand request, CancellationToken cancellationToken)
        {
            IndividualSupplierCreateModel individualSupplierCreateModel = new IndividualSupplierCreateModel();
            individualSupplierCreateModel.FirstName = request.FirstName;
            individualSupplierCreateModel.LastName = request.LastName;
            individualSupplierCreateModel.EmailAddress = request.EmailAddress;
            individualSupplierCreateModel.JobPosition = request.JobPosition;
            individualSupplierCreateModel.Title = request.Title;
            individualSupplierCreateModel.CompanyName = request.CompanyName;
            individualSupplierCreateModel.TaxId = request.TaxId;

            var result = _iIndividualSupplierCommandService.CreateIndividualSupplier(individualSupplierCreateModel);
            return Task.FromResult(true);
        }
    }
}
