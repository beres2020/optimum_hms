﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.Inventory.Application.Supplier.Commands
{
    public class CreateSupplierCommand : IRequest<bool>
    {
        public string Name { get; set; }
        public string TaxId { get; set; }
    }
}
