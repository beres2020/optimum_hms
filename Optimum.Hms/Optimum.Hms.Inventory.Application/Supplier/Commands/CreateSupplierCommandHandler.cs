﻿using MediatR;
using Optimum.Hms.Inventory.Common.Services.Supplier;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Optimum.Hms.Inventory.Application.Supplier.Commands
{
    public class CreateSupplierCommandHandler : IRequestHandler<CreateSupplierCommand, bool>
    {
        private readonly ISupplierCommandService _supplierCommandService;
        public CreateSupplierCommandHandler(ISupplierCommandService supplierCommandService)
        {
            _supplierCommandService = supplierCommandService;
        }
        public Task<bool> Handle(CreateSupplierCommand request, CancellationToken cancellationToken)
        {
            SupplierCreateModel supplierCreateModel = new SupplierCreateModel();
            supplierCreateModel.Name = request.Name;
            supplierCreateModel.TaxId = request.TaxId;

            var result = _supplierCommandService.CreateSupplier(supplierCreateModel);
            return Task.FromResult(true);
        }
    }
}
