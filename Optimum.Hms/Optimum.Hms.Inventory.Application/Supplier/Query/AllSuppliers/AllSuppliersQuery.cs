using System.Collections;
using System.Collections.Generic;
using MediatR;
using Optimum.Hms.Inventory.Common.Services.Supplier;

namespace Optimum.Hms.Inventory.Application.Supplier.Query.AllSuppliers
{
    public class AllSuppliersQuery : IRequest<IEnumerable<SupplierResultModel>>
    {
        
    }
}