using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Optimum.Hms.Inventory.Common.Services.Supplier;

namespace Optimum.Hms.Inventory.Application.Supplier.Query.AllSuppliers
{
    public class AllSuppliersQueryHandler : IRequestHandler<AllSuppliersQuery, IEnumerable<SupplierResultModel>>
    {
        private readonly ISupplierQueryService _supplierQueryService;

        public AllSuppliersQueryHandler(ISupplierQueryService supplierQueryService)
        {
            _supplierQueryService = supplierQueryService;
        }
        public Task<IEnumerable<SupplierResultModel>> Handle(AllSuppliersQuery request, CancellationToken cancellationToken)
        {
            var result = _supplierQueryService.AllSuppliers();
            return Task.FromResult(result);
        }
    }
}