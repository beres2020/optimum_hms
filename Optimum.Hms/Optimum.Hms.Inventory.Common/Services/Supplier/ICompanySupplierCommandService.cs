﻿using Optimum.Hms.Inventory.Common.Services.Supplier.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.Inventory.Common.Services.Supplier
{
    public interface ICompanySupplierCommandService
    {
        public bool CreateCompanySupplier(CompanySupplierCreateModel companySupplierCreateModel);
    }
}
