﻿using Optimum.Hms.Inventory.Common.Services.Supplier.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.Inventory.Common.Services.Supplier
{
    public interface IIndividualSupplierCommandService
    {
        public bool CreateIndividualSupplier(IndividualSupplierCreateModel individualSupplierCreateModel);
    }
}
