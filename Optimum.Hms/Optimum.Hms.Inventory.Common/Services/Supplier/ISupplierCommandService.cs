﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.Inventory.Common.Services.Supplier
{
    public interface ISupplierCommandService
    {
        public bool CreateSupplier(SupplierCreateModel supplierCreateModel);
    }
}
