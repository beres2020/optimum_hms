using System.Collections;
using System.Collections.Generic;

namespace Optimum.Hms.Inventory.Common.Services.Supplier
{
    public interface ISupplierQueryService
    {
        public IEnumerable<SupplierResultModel> AllSuppliers();
    }
}