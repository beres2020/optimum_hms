﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.Inventory.Common.Services.Supplier
{
    public class SupplierCreateModel
    {
        public string Name { get; set; }
        public string TaxId { get; set; }

        public SupplierCreateModel()
        { }
    }
}
