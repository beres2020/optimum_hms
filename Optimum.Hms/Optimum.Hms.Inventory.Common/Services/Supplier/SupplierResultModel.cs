namespace Optimum.Hms.Inventory.Common.Services.Supplier
{
    public class SupplierResultModel
    {
        public string Name { get; set; }
        public string TypeOfSupplier { get; set; }
        public string TaxId { get; set; }
        public string Comment { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
    }
}