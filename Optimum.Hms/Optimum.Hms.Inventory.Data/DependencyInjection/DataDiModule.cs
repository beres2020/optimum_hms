using Autofac;
using Optimum.Hms.Inventory.Common.Services.Supplier;
using Optimum.Hms.Inventory.Data.Services.Supplier;

namespace Optimum.Hms.Inventory.Data.DependencyInjection
{
    public class DataDiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SupplierQueryService>().As<ISupplierQueryService>();
            builder.RegisterType<SupplierCommandService>().As<ISupplierCommandService>();
            builder.RegisterType<CompanySupplierCommandService>().As<ICompanySupplierCommandService>();
            builder.RegisterType<IndividualSupplierCommandService>().As<IIndividualSupplierCommandService>();
        } 
    }
}