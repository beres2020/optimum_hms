﻿using Optimum.Hms.Inventory.Common.Services.Supplier;
using Optimum.Hms.Inventory.Common.Services.Supplier.Models;
using Optimum.Hms.Inventory.Domain.Entities.SupplierEntities;
using Optimum.Hms.SharedKernel.CompanyModels;
using Optimum.Hms.SharedKernel.ContactModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.Inventory.Data.Services.Supplier
{
    class CompanySupplierCommandService : ICompanySupplierCommandService
    {
        private readonly ISupplierRepository _supplierRepository;

        public CompanySupplierCommandService(ISupplierRepository supplierRepository)
        {
            _supplierRepository = supplierRepository;
        }

        public bool CreateCompanySupplier(CompanySupplierCreateModel companySupplierCreateModel)
        {
            CompanySupplier companySupplier = new CompanySupplier();
            companySupplier.Name = companySupplierCreateModel.CompanyName;
            companySupplier.ContactInfo = new Contact();
            companySupplier.ContactInfo.Email = new Email();
            companySupplier.ContactInfo.Email.EmailAddress = companySupplierCreateModel.EmailAddress;
            companySupplier.ContactInfo.Address = new Address();
            companySupplier.ContactInfo.Address.AddressLine1 = companySupplierCreateModel.AddressLine1;
            companySupplier.ContactInfo.Address.AddressLine2 = companySupplierCreateModel.AddressLine2;
            companySupplier.ContactInfo.Address.City = companySupplierCreateModel.City;
            companySupplier.ContactInfo.Address.State = companySupplierCreateModel.State;
            companySupplier.ContactInfo.Address.Zip = companySupplierCreateModel.Zip;
            companySupplier.ContactInfo.Address.Country = companySupplierCreateModel.Country;
            companySupplier.ContactInfo.Phone = new List<Phone>();

            for (int count = 0; count < companySupplierCreateModel.Phones.Length; count++)
            {
                Phone phone = new Phone();
                phone.PhoneNumber = companySupplierCreateModel.Phones[count];
                phone.PhoneCategory = companySupplierCreateModel.PhoneCategories[count];
                companySupplier.ContactInfo.Phone.Add(phone);
            }

            companySupplier.Comment = companySupplierCreateModel.Comment;
            companySupplier.Contacts = new List<CompanyContact>();
            companySupplier.TaxId = companySupplierCreateModel.TaxId;

            var result = _supplierRepository.CreateCompanySupplier(companySupplier);
            return result;
        }
    }
}
