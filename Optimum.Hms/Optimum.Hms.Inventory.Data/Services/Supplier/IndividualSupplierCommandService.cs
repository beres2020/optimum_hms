﻿using Optimum.Hms.Inventory.Common.Services.Supplier;
using Optimum.Hms.Inventory.Common.Services.Supplier.Models;
using Optimum.Hms.Inventory.Domain.Entities.SupplierEntities;
using Optimum.Hms.SharedKernel.CompanyModels;
using Optimum.Hms.SharedKernel.ContactModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.Inventory.Data.Services.Supplier
{
    class IndividualSupplierCommandService : IIndividualSupplierCommandService
    {
        private readonly ISupplierRepository _supplierRepository;

        public IndividualSupplierCommandService(ISupplierRepository supplierRepository)
        {
            _supplierRepository = supplierRepository;
        }

        public bool CreateIndividualSupplier(IndividualSupplierCreateModel individualSupplierCreateModel)
        {
            IndividualSupplier individualSupplier = new IndividualSupplier();
            individualSupplier.FirstName = individualSupplierCreateModel.FirstName;
            individualSupplier.LastName = individualSupplierCreateModel.LastName;
            individualSupplier.ContactInfo = new Contact();
            individualSupplier.ContactInfo.Email = new Email();
            individualSupplier.ContactInfo.Email.EmailAddress = individualSupplierCreateModel.EmailAddress;
            individualSupplier.JobPosition = individualSupplierCreateModel.JobPosition;
            individualSupplier.Title = individualSupplierCreateModel.Title;
            individualSupplier.Company = new CompanySupplier();
            individualSupplier.Company.Name = individualSupplierCreateModel.CompanyName;
            individualSupplier.Company.TaxId = individualSupplierCreateModel.TaxId;
            individualSupplier.Company.Comment = individualSupplierCreateModel.Comment;
            individualSupplier.Company.ContactInfo = new Contact();
            individualSupplier.Contacts = new List<CompanyContact>();


            var result = _supplierRepository.CreateIndividualSupplier(individualSupplier);
            return result;
        }
    }
}
