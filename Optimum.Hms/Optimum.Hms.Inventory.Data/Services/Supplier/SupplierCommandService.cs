﻿using Optimum.Hms.Inventory.Common.Services.Supplier;
using Optimum.Hms.Inventory.Domain.Entities.SupplierEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.Inventory.Data.Services.Supplier
{
    class SupplierCommandService : ISupplierCommandService
    {
        private readonly ISupplierRepository _supplierRepository;

        public SupplierCommandService(ISupplierRepository supplierRepository)
        {
            _supplierRepository = supplierRepository;
        }

        public bool CreateSupplier(SupplierCreateModel supplierCreateModel)
        {
            CompanySupplier companySupplier = new CompanySupplier();
            companySupplier.Name = supplierCreateModel.Name;

            var result = _supplierRepository.CreateSupplier(companySupplier);
            return result;
        }
    }
}
