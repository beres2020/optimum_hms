using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Optimum.Hms.Inventory.Common.Services.Supplier;
using Optimum.Hms.Inventory.Domain.Entities.Enumerations;
using Optimum.Hms.Inventory.Domain.Entities.SupplierEntities;
using Optimum.Hms.Inventory.Domain.Interfaces;

namespace Optimum.Hms.Inventory.Data.Services.Supplier
{
    public class SupplierQueryService : ISupplierQueryService
    {
        private readonly ISupplierRepository _supplierRepository;

        public SupplierQueryService(ISupplierRepository supplierRepository)
        {
            _supplierRepository = supplierRepository;
        }
        public IEnumerable<SupplierResultModel> AllSuppliers()
        {
            var result = _supplierRepository.GetAllSuppliers();
            var dto = new List<SupplierResultModel>();
            foreach (var supplier in result)
            {
                if (supplier is CompanySupplier e)
                {
                    dto.Add(new SupplierResultModel()
                    {
                        Name = e.Name,
                        TypeOfSupplier = SupplierType.Company.Name,
                        TaxId = e.TaxId,
                        Comment = e.Comment,
                        //EmailAddress = e.ContactInfo.Email.EmailAddress
                    });
                }
                else
                {
                    var indSupp = supplier as IndividualSupplier;
                    dto.Add(new SupplierResultModel()
                    {
                        Name = indSupp?.FirstName + indSupp?.LastName,
                        TypeOfSupplier = SupplierType.Individual.Name,
                        //EmailAddress = indSupp.ContactInfo.Email.EmailAddress
                    });
                }
            }

            return dto;
           
        }
    }
}