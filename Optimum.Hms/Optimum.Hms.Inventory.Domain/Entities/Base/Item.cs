using System;
using Optimum.Hms.Inventory.Domain.Interfaces;
using Optimum.Hms.SharedKernel.SeedWork;

namespace Optimum.Hms.Inventory.Domain.Entities.Base
{
    public abstract class Item : Entity, IAuditable
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string Category { get; set; }

        public string Barcode { get; set; }

        public bool IsDisabled { get; set; }
        public bool CanBeSold { get; set; }
        public bool CanBePurchased { get; set; }

        public UnitOfMeasure UnitOfMeasure { get; set; }
        
        
        
        
        public DateTime Created { get; }
        public Guid CreatedBy { get; }
        public DateTime Updated { get; set; }
        public Guid UpdatedBy { get; set; }
    }

  
}