using Optimum.Hms.SharedKernel.SeedWork;

namespace Optimum.Hms.Inventory.Domain.Entities.Enumerations
{
    public class ItemType : Enumeration
    {
        public static ItemType Product = new ItemType(1, nameof(Product).ToLowerInvariant());
        public static ItemType Service = new ItemType(2, nameof(Service).ToLowerInvariant());
        public static ItemType FixedAsset = new ItemType(3, nameof(FixedAsset).ToLowerInvariant());
        public ItemType(int id, string name) : base(id, name)
        {
        }
    }
}