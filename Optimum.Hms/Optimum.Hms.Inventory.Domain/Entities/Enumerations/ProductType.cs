using System;
using System.Collections.Generic;
using System.Linq;
using Optimum.Hms.Inventory.Domain.Exceptions;
using Optimum.Hms.SharedKernel.SeedWork;

namespace Optimum.Hms.Inventory.Domain.Entities.Enumerations
{
    public class ProductType : Enumeration
    {
        public static ProductType Consumable = new ProductType(1, nameof(Consumable).ToLowerInvariant());
        public static ProductType StorableProduct = new ProductType(2, nameof(StorableProduct).ToLowerInvariant());
        public ProductType(int id, string name) : base(id, name)
        {
        }
        
        public static IEnumerable<ProductType> List() =>
            new[] { Consumable, StorableProduct };
        
        public static ProductType FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
            {
                throw new InventoryDomainException($"Possible values for ProductType: {string.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }

        public static ProductType From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
            {
                throw new InventoryDomainException($"Possible values for ProductType: {string.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }
    }
}