using Optimum.Hms.SharedKernel.SeedWork;

namespace Optimum.Hms.Inventory.Domain.Entities.Enumerations
{
    public class SupplierType : Enumeration
    {
        public static SupplierType Individual = new SupplierType(1, nameof(Individual).ToLowerInvariant());
        public static SupplierType Company = new SupplierType(2, nameof(Company).ToLowerInvariant());
        public SupplierType(int id, string name) : base(id, name)
        {
        }
    }
}