using Optimum.Hms.Inventory.Domain.Entities.Base;
using Optimum.Hms.Inventory.Domain.Entities.Enumerations;
using Optimum.Hms.Inventory.Domain.Interfaces;

namespace Optimum.Hms.Inventory.Domain.Entities.ItemEntities
{
    public class Product : Item, IPurchasableItem, ISellableItem // Stock Item
    {
        public ProductType ProductType { get; set; }
        

        public Product()
        {
            
        }
    }
}