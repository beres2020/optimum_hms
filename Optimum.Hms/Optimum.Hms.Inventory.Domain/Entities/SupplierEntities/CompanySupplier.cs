using System.Collections.Generic;
using Optimum.Hms.Inventory.Domain.Entities.Enumerations;
using Optimum.Hms.Inventory.Domain.Interfaces;
using Optimum.Hms.SharedKernel.CompanyModels;

namespace Optimum.Hms.Inventory.Domain.Entities.SupplierEntities
{
    public class CompanySupplier : Company, ISupplier
    {
        public List<CompanyContact> Contacts { get; set; }
        public SupplierType SupplierType { get; set; }
        public void setId(string id)
        {
            Id = id;
        }
       
    }
}