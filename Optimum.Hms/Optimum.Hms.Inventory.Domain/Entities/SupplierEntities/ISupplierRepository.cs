using System.Collections;
using System.Collections.Generic;
using Optimum.Hms.Inventory.Domain.Interfaces;

namespace Optimum.Hms.Inventory.Domain.Entities.SupplierEntities
{
    public interface ISupplierRepository
    {
        public IEnumerable<ISupplier> GetAllSuppliers();
        public bool CreateSupplier(CompanySupplier companySupplier);
        public bool CreateCompanySupplier(CompanySupplier companySupplier);
        public bool CreateIndividualSupplier(IndividualSupplier individualSupplier);
    }
}