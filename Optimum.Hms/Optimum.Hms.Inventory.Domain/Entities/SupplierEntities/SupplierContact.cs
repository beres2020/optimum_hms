using Optimum.Hms.SharedKernel.ContactModels;

namespace Optimum.Hms.Inventory.Domain.Entities.SupplierEntities
{
    public class SupplierContact
    {
        public string Name { get; set; }
        public Contact Contact { get; set; }
    }
}