using System;

namespace Optimum.Hms.Inventory.Domain.Interfaces
{
    public interface IAuditable
    {
        public DateTime Created { get; }
        public Guid CreatedBy { get; }
        public DateTime Updated { get; set; }
        public Guid UpdatedBy { get; set; }
    }
}