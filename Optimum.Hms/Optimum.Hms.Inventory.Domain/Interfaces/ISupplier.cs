using System.Collections.Generic;
using Optimum.Hms.Inventory.Domain.Entities.Enumerations;
using Optimum.Hms.SharedKernel.CompanyModels;

namespace Optimum.Hms.Inventory.Domain.Interfaces
{
    public interface ISupplier
    {
        public List<CompanyContact> Contacts { get; set; }
        public SupplierType SupplierType { get; set; }
    }
}