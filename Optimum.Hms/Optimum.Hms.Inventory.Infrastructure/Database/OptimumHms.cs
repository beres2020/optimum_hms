﻿using MongoDB.Bson;
using MongoDB.Driver;
using Optimum.Hms.Inventory.Domain.Entities.SupplierEntities;
using Optimum.Hms.Inventory.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.Inventory.Infrastructure.Database
{
    class OptimumHms
    {
        private MongoClient _client = new MongoClient("mongodb+srv://beresford:yTQ5GpvZIIMV1mdL@asc1.cnfyx.mongodb.net/optimum_hms?retryWrites=true&w=majority");
        private IMongoDatabase _database;
        private IMongoCollection<SupplierDbModel> _supplierCollection;
        private IMongoCollection<CompanyDbModel> _companySupplierCollection;
        private IMongoCollection<IndividualDbModel> _individualSupplierCollection;

        public OptimumHms()
        {
            _database = _client.GetDatabase("optimum_hms");
            _supplierCollection = _database.GetCollection<SupplierDbModel>("suppliers");
            _companySupplierCollection = _database.GetCollection<CompanyDbModel>("companies");
            _individualSupplierCollection = _database.GetCollection<IndividualDbModel>("individuals");
        }

        public void createRecord(SupplierDbModel record)
        {
            _supplierCollection.InsertOne(record);
        }

        public void createCompanyRecord(CompanyDbModel companyDbModel)
        {
            //_companySupplierCollection.InsertOne(companySupplier);
            _companySupplierCollection.InsertOne(companyDbModel);
        }

        public void createIndividualRecord(IndividualDbModel individualDbModel)
        {
            //_individualSupplierCollection.InsertOne(individualSupplier);
            _individualSupplierCollection.InsertOne(individualDbModel);
        }

        public IEnumerable<SupplierDbModel> getAllSuppliers()
        {
            return _supplierCollection.AsQueryable<SupplierDbModel>().ToList();
        }
        public IEnumerable<CompanyDbModel> getAllCompanies()
        {
            return _companySupplierCollection.AsQueryable<CompanyDbModel>().ToList();
        }
        public IEnumerable<IndividualDbModel> getAllIndividuals()
        {
            return _individualSupplierCollection.AsQueryable<IndividualDbModel>().ToList();
        }
        public CompanyDbModel getCompany(string id)
        {
            ObjectId record_id = new ObjectId(id);
            return _companySupplierCollection.Find(x => x.Id == record_id).SingleOrDefault();
        }
        public IndividualDbModel getIndividual(string id)
        {
            ObjectId record_id = new ObjectId(id);
            return _individualSupplierCollection.Find(x => x.Id == record_id).SingleOrDefault();
        }
    }
}
