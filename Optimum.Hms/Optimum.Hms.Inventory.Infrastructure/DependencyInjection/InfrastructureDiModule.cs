using Autofac;
using Optimum.Hms.Inventory.Domain.Entities.SupplierEntities;
using Optimum.Hms.Inventory.Infrastructure.Repositories;

namespace Optimum.Hms.Inventory.Infrastructure.DependencyInjection
{
    public class InfrastructureDiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SupplierRepository>().As<ISupplierRepository>();
        }
    }
}