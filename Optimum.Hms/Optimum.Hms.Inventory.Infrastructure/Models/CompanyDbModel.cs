﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Optimum.Hms.SharedKernel.CompanyModels;
using Optimum.Hms.SharedKernel.ContactModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.Inventory.Infrastructure.Models
{
    class CompanyDbModel
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public int SupplierTypeId { get; set; }
        public string Name { get; set; }
        public Contact ContactInfo { get; set; }
        public string TaxId { get; set; }
        public string Comment { get; set; }
        public CompanyContact[] CompanyContacts { get; set; }
    }
}
