﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Optimum.Hms.SharedKernel.CompanyModels;
using Optimum.Hms.SharedKernel.ContactModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.Inventory.Infrastructure.Models
{
    class IndividualDbModel
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public int SupplierTypeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Contact ContactInfo { get; set; }
        public string JobPosition { get; set; }
        public string Title { get; set; }
        //public string Comment { get; set; }
        public CompanyContact[] CompanyContacts { get; set; }
        public ObjectId CompanyId { get; set; }
    }
}
