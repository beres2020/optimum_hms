﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Optimum.Hms.SharedKernel.CompanyModels;
using Optimum.Hms.SharedKernel.ContactModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.Inventory.Infrastructure.Models
{
    [BsonIgnoreExtraElements]
    class SupplierDbModel
    {
        [BsonId]
        public ObjectId Id { get; set; }
        //[BsonElement("supplier_type_id")]
        public int SupplierTypeId { get; set; }
        //[BsonElement("first_name")]
        public string FirstName { get; set; }
        //[BsonElement("last_name")]
        public string LastName { get; set; }
        //[BsonElement("contact_info")]
        public Contact ContactInfo { get; set; }
        //[BsonElement("job_position")]
        public string JobPosition { get; set; }
        //[BsonElement("title")]
        public string Title { get; set; }
        //[BsonElement("company_name")]
        public string CompanyName { get; set; }
        //[BsonElement("tax_id")]
        public string TaxId { get; set; }
        //[BsonElement("comment")]
        public string Comment { get; set; }
        //[BsonElement("company_contacts")]
        public CompanyContact[] CompanyContacts { get; set; }
    }
}
