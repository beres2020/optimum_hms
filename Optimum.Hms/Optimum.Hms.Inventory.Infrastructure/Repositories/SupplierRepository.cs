using System.Collections.Generic;
using Optimum.Hms.Inventory.Domain.Entities.Enumerations;
using Optimum.Hms.Inventory.Domain.Entities.SupplierEntities;
using Optimum.Hms.Inventory.Domain.Interfaces;
using Optimum.Hms.Inventory.Infrastructure.Database;
using Optimum.Hms.Inventory.Infrastructure.Models;
using Optimum.Hms.SharedKernel.CompanyModels;
using Optimum.Hms.SharedKernel.ContactModels;

namespace Optimum.Hms.Inventory.Infrastructure.Repositories
{
    public class SupplierRepository : ISupplierRepository
    {
        private OptimumHms _optimumHms = new OptimumHms();
        public IEnumerable<ISupplier> GetAllSuppliers()
        {
            //DataDao -> Domain

            //IEnumerable<SupplierDbModel> suppliers = _optimumHms.getAllSuppliers();
            IEnumerable<CompanyDbModel> companies = _optimumHms.getAllCompanies();
            IEnumerable<IndividualDbModel> individuals = _optimumHms.getAllIndividuals();

            var result = new List<ISupplier>();

            foreach (CompanyDbModel model in companies)
            {
                CompanySupplier companySupplier = new CompanySupplier();
                companySupplier.setId(model.Id.ToString());
                companySupplier.Name = model.Name;
                companySupplier.TaxId = model.TaxId;
                companySupplier.Comment = model.Comment;
                companySupplier.SupplierType = SupplierType.Company;
                companySupplier.ContactInfo = model.ContactInfo;
                /*
                companySupplier.ContactInfo = new Contact();
                companySupplier.ContactInfo.Email = new Email();
                companySupplier.ContactInfo.Email.EmailAddress = model.ContactInfo.Email.EmailAddress;
                companySupplier.ContactInfo.Email.EmailCategory = model.ContactInfo.Email.EmailCategory;
                companySupplier.ContactInfo.Address = new Address();
                companySupplier.ContactInfo.Address.AddressLine1 = model.ContactInfo.Address.AddressLine1;
                companySupplier.ContactInfo.Address.AddressLine2 = model.ContactInfo.Address.AddressLine2;
                companySupplier.ContactInfo.Address.City = model.ContactInfo.Address.City;
                companySupplier.ContactInfo.Address.State = model.ContactInfo.Address.State;
                companySupplier.ContactInfo.Address.Zip = model.ContactInfo.Address.Zip;
                companySupplier.ContactInfo.Address.Country = model.ContactInfo.Address.Country;
                companySupplier.ContactInfo.Phone = new Phone();
                companySupplier.ContactInfo.Phone.PhoneNumber = model.ContactInfo.Phone.PhoneNumber;
                companySupplier.ContactInfo.Phone.PhoneCategory = model.ContactInfo.Phone.PhoneCategory;
                companySupplier.ContactInfo.Comment = model.ContactInfo.Comment;
                */
                companySupplier.Contacts = new List<CompanyContact>();

                for (int count = 0; count < model.CompanyContacts.Length; count++)
                {
                    companySupplier.Contacts.Add(model.CompanyContacts[count]);
                }

                result.Add(companySupplier);
            }

            foreach (IndividualDbModel model in individuals)
            {
                IndividualSupplier individualSupplier = new IndividualSupplier();
                individualSupplier.setId(model.Id.ToString());
                individualSupplier.FirstName = model.FirstName;
                individualSupplier.LastName = model.LastName;
                individualSupplier.JobPosition = model.JobPosition;
                individualSupplier.Title = model.Title;
                individualSupplier.SupplierType = SupplierType.Individual;
                individualSupplier.ContactInfo = model.ContactInfo;
                /*
                individualSupplier.ContactInfo = new Contact();
                individualSupplier.ContactInfo.Email = new Email();
                individualSupplier.ContactInfo.Email.EmailAddress = model.ContactInfo.Email.EmailAddress;
                individualSupplier.ContactInfo.Email.EmailCategory = model.ContactInfo.Email.EmailCategory;
                individualSupplier.ContactInfo.Address = new Address();
                individualSupplier.ContactInfo.Address.AddressLine1 = model.ContactInfo.Address.AddressLine1;
                individualSupplier.ContactInfo.Address.AddressLine2 = model.ContactInfo.Address.AddressLine2;
                individualSupplier.ContactInfo.Address.City = model.ContactInfo.Address.City;
                individualSupplier.ContactInfo.Address.State = model.ContactInfo.Address.State;
                individualSupplier.ContactInfo.Address.Zip = model.ContactInfo.Address.Zip;
                individualSupplier.ContactInfo.Address.Country = model.ContactInfo.Address.Country;
                individualSupplier.ContactInfo.Phone = new Phone();
                individualSupplier.ContactInfo.Phone.PhoneNumber = model.ContactInfo.Phone.PhoneNumber;
                individualSupplier.ContactInfo.Phone.PhoneCategory = model.ContactInfo.Phone.PhoneCategory;
                individualSupplier.ContactInfo.Comment = model.ContactInfo.Comment;
                */
                //individualSupplier.Company = this.GetCompanySupplier(model.Id.ToString());
                individualSupplier.Contacts = new List<CompanyContact>();

                for (int count = 0; count < model.CompanyContacts.Length; count++)
                {
                    individualSupplier.Contacts.Add(model.CompanyContacts[count]);
                }

                result.Add(individualSupplier);
            }
            
            return result;
        }

        public CompanySupplier GetCompanySupplier(string id)
        {
            CompanyDbModel company = _optimumHms.getCompany(id);
            CompanySupplier companySupplier = new CompanySupplier();
            companySupplier.setId(company.Id.ToString());
            companySupplier.Name = company.Name;
            companySupplier.TaxId = company.TaxId;
            companySupplier.Comment = company.Comment;
            companySupplier.SupplierType = SupplierType.Company;
            companySupplier.ContactInfo = company.ContactInfo;
            /*
            companySupplier.ContactInfo = new Contact();
            companySupplier.ContactInfo.Email = new Email();
            companySupplier.ContactInfo.Email.EmailAddress = company.ContactInfo.Email.EmailAddress;
            companySupplier.ContactInfo.Email.EmailCategory = company.ContactInfo.Email.EmailCategory;
            companySupplier.ContactInfo.Address = new Address();
            companySupplier.ContactInfo.Address.AddressLine1 = company.ContactInfo.Address.AddressLine1;
            companySupplier.ContactInfo.Address.AddressLine2 = company.ContactInfo.Address.AddressLine2;
            companySupplier.ContactInfo.Address.City = company.ContactInfo.Address.City;
            companySupplier.ContactInfo.Address.State = company.ContactInfo.Address.State;
            companySupplier.ContactInfo.Address.Zip = company.ContactInfo.Address.Zip;
            companySupplier.ContactInfo.Address.Country = company.ContactInfo.Address.Country;
            companySupplier.ContactInfo.Phone = new Phone();
            companySupplier.ContactInfo.Phone.PhoneNumber = company.ContactInfo.Phone.PhoneNumber;
            companySupplier.ContactInfo.Phone.PhoneCategory = company.ContactInfo.Phone.PhoneCategory;
            companySupplier.ContactInfo.Comment = company.ContactInfo.Comment;
            */
            companySupplier.Contacts = new List<CompanyContact>();

            for (int count = 0; count < company.CompanyContacts.Length; count++)
            {
                companySupplier.Contacts.Add(company.CompanyContacts[count]);
            }

            return companySupplier;
        }

        public ISupplier GetSupplier(string id, int supplierType)
        {
            OptimumHms optimumHms = new OptimumHms();

            switch(supplierType)
            {
                //case SupplierType.Individual.Id:
                //break;
                case 11:
                    break;
            }
            CompanyDbModel company = optimumHms.getCompany(id);
            return (ISupplier)company;
        }

        public bool CreateSupplier(CompanySupplier companySupplier)
        {
            SupplierDbModel supplierModel = new SupplierDbModel();
            supplierModel.CompanyName = companySupplier.Name;

            OptimumHms optimumHms = new OptimumHms();
            _optimumHms.createRecord(supplierModel);
            var result = true;
            return result;
        }

        public bool CreateCompanySupplier(CompanySupplier companySupplier)
        {
            CompanyDbModel companyDbModel = new CompanyDbModel();
            companyDbModel.Name = companySupplier.Name;
            companyDbModel.TaxId = companySupplier.TaxId;
            companyDbModel.Comment = companySupplier.Comment;
            companyDbModel.ContactInfo = companySupplier.ContactInfo;
            companyDbModel.CompanyContacts = companySupplier.Contacts.ToArray();

            _optimumHms.createCompanyRecord(companyDbModel);
            var result = true;
            return result;
        }

        public bool CreateIndividualSupplier(IndividualSupplier individualSupplier)
        {
            IndividualDbModel individualDbModel = new IndividualDbModel();
            individualDbModel.FirstName = individualSupplier.FirstName;
            individualDbModel.LastName = individualSupplier.LastName;
            individualDbModel.JobPosition = individualSupplier.JobPosition;
            individualDbModel.Title = individualSupplier.Title;
            individualDbModel.CompanyContacts = individualSupplier.Contacts.ToArray();

            _optimumHms.createIndividualRecord(individualDbModel);
            var result = true;
            return result;
        }
    }
}