using Optimum.Hms.SharedKernel.ContactModels;
using Optimum.Hms.SharedKernel.SeedWork;

namespace Optimum.Hms.SharedKernel.CompanyModels
{
    
    public abstract class Company : Entity
    {
        public string Name { get; set; }
        public Contact ContactInfo { get; set; }
        public string TaxId { get; set; }
        public string Comment { get; set; }
    }
    
    
}