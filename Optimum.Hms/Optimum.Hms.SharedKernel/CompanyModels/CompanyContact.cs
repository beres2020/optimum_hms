using Optimum.Hms.SharedKernel.ContactModels;

namespace Optimum.Hms.SharedKernel.CompanyModels
{
    public class CompanyContact
    {
        public string Name { get; set; }
        public Contact Contact { get; set; }
        //add type of contact
    }
}