﻿using Optimum.Hms.SharedKernel.ContactModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Optimum.Hms.SharedKernel.CompanyModels
{
    class CompanyContactPerson
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string JobPosition { get; set; }
        public Email EmailAddress { get; set; }
        public Phone[] Phones { get; set; }
    }
}
