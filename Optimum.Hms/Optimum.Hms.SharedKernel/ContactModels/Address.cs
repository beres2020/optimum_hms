using System.Collections.Generic;
using Optimum.Hms.SharedKernel.SeedWork;

namespace Optimum.Hms.SharedKernel.ContactModels
{
    public class Address : ValueObject
    {
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        protected override IEnumerable<object> GetEqualityComponents()
        {
            throw new System.NotImplementedException();
        }
    }
}