using System.Collections.Generic;
using Optimum.Hms.SharedKernel.SeedWork;

namespace Optimum.Hms.SharedKernel.ContactModels
{
    public class Contact : ValueObject
    {
        public Email Email { get; set; }
        public Address Address { get; set; }
        public List<Phone> Phone { get; set; }
        public string Comment { get; set; }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            throw new System.NotImplementedException();
        }
    }
}