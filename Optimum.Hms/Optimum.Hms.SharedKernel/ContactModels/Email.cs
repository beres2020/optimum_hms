using System.Collections.Generic;
using Optimum.Hms.SharedKernel.SeedWork;

namespace Optimum.Hms.SharedKernel.ContactModels
{
    public class Email : ValueObject
    {
        public string EmailAddress { get; set; }
        public string EmailCategory { get; set; } //or Email Type.. personal, work, etc or could system enums
        
        protected override IEnumerable<object> GetEqualityComponents()
        {
            throw new System.NotImplementedException();
        }
    }
}