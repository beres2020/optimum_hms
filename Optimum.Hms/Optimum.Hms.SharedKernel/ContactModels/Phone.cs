namespace Optimum.Hms.SharedKernel.ContactModels
{
    public class Phone
    {
        public string PhoneNumber { get; set; }
        public string PhoneCategory { get; set; } //Mobile, landline, etc.
    }
}