using Optimum.Hms.SharedKernel.ContactModels;
using Optimum.Hms.SharedKernel.SeedWork;

namespace Optimum.Hms.SharedKernel.PersonModels
{
    public class Person : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Contact ContactInfo { get; set; }
        public string JobPosition { get; set; }
        public string Title { get; set; }
    }
}